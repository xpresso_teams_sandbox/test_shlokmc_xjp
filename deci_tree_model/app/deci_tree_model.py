import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    strat_train_set = pickle.load(open(f"{PICKLE_PATH}/strat_train_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    strat_test_set = pickle.load(open(f"{PICKLE_PATH}/strat_test_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    strat_train_x = pickle.load(open(f"{PICKLE_PATH}/strat_train_x.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_labels = pickle.load(open(f"{PICKLE_PATH}/housing_labels.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_num = pickle.load(open(f"{PICKLE_PATH}/housing_num.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_cat = pickle.load(open(f"{PICKLE_PATH}/housing_cat.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_prepared = pickle.load(open(f"{PICKLE_PATH}/housing_prepared.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    lin_reg = pickle.load(open(f"{PICKLE_PATH}/lin_reg.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    tree_reg = pickle.load(open(f"{PICKLE_PATH}/tree_reg.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = deci_tree_model
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["strat_train_set", "strat_test_set","strat_train_x", "housing_labels", "housing_num", "housing_cat", "housing_prepared", "lin_reg", "tree_reg"]



tree_reg = DecisionTreeRegressor(random_state=42)
tree_reg.fit(housing_prepared, housing_labels)
housing_predictions = tree_reg.predict(housing_prepared)
tree_mse = mean_squared_error(housing_labels, housing_predictions)
tree_rmse = np.sqrt(tree_mse)
tree_rmse

try:
    pickle.dump(strat_train_set, open(f"{PICKLE_PATH}/strat_train_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(strat_test_set, open(f"{PICKLE_PATH}/strat_test_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(strat_train_x, open(f"{PICKLE_PATH}/strat_train_x.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_labels, open(f"{PICKLE_PATH}/housing_labels.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_num, open(f"{PICKLE_PATH}/housing_num.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_cat, open(f"{PICKLE_PATH}/housing_cat.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_prepared, open(f"{PICKLE_PATH}/housing_prepared.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(lin_reg, open(f"{PICKLE_PATH}/lin_reg.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(tree_reg, open(f"{PICKLE_PATH}/tree_reg.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

